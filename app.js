// 1.Перебирає масив та виконує певну функцію для кожного його елемента
// 2.Задати масиву довжину 0
// 3.Array.isArray()


function filterBy(arr, dataType) {
    let newArr = arr.filter(item => typeof item !== dataType)
    return newArr;
}

const arr = ['hello', 'world', 23, '23', null, true];
const allTypes = ["string", "number", "boolean"];
allTypes.forEach(type => console.log(filterBy(arr, type)));
